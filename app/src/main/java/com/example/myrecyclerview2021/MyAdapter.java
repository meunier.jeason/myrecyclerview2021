package com.example.myrecyclerview2021;

import android.app.AlertDialog;
import android.text.style.BackgroundColorSpan;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import androidx.annotation.NonNull;
import androidx.recyclerview.widget.RecyclerView;

import java.util.ArrayList;

public class MyAdapter extends RecyclerView.Adapter<MyAdapter.MyViewHolder> {

    private static ArrayList<Prof> profs;

    public MyAdapter(ArrayList<Prof> list){
        this.profs = list;
    }

    public static ArrayList<Prof> getProfs(){
        return profs;
    }

    public void setProfs(ArrayList<Prof> lesProfs) { this.profs = lesProfs; }

    public static class MyViewHolder extends RecyclerView.ViewHolder implements View.OnLongClickListener {

        private TextView tvName;
        private TextView tvMat;
        private Prof current;
        ItemLongClickListener itemLongClickListener;

        public MyViewHolder(@NonNull View itemView) {
            super(itemView);
            tvName = itemView.findViewById(R.id.textViewNomProf);
            tvMat = itemView.findViewById(R.id.textViewMatiereProf);
            itemView.setOnLongClickListener(this);
            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    Log.d("MesLogs", "je clique sur "+itemView);
                    new AlertDialog.Builder(itemView.getContext())
                            .setTitle(current.getNom())
                            .setMessage(current.getMatiere())
                            .show();
                }
            });
        }

        public void display(Prof prof){
            current = prof;
            tvName.setText(prof.getNom());
            tvMat.setText(prof.getMatiere());

        }
        public void setItemLongClickListener(ItemLongClickListener item){
            this.itemLongClickListener = item;

        }
        @Override
        public boolean onLongClick(View v) {
            this.itemLongClickListener.onItemLongClick(v,getLayoutPosition());
            return false;
        }
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        // Creer une nouvelle view
        LayoutInflater layoutInflater = LayoutInflater.from(parent.getContext());
        View view = layoutInflater.inflate(R.layout.cells, parent, false);
        MyViewHolder myViewHolder = new MyViewHolder(view);
        return myViewHolder;
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        Prof p = profs.get(position);
        holder.display(p);
        holder.setItemLongClickListener(new ItemLongClickListener() {
            @Override
            public void onItemLongClick(View v, int pos) {
                profs.remove(pos);
                Log.d("MesLogs","LongClick");
                notifyDataSetChanged();
            }
        });
    }

    // Utiliser pour retourner le nb d'elements de la RecyclerView
    @Override
    public int getItemCount() {
        return profs.size();
    }

}
