package com.example.myrecyclerview2021;

import androidx.appcompat.app.AppCompatActivity;

import android.content.Intent;
import android.os.Bundle;
import android.util.Log;
import android.view.View;
import android.widget.EditText;

public class ActivityAddProf extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_add_prof);
    }

    public void validerProf(View view) {
        EditText name = findViewById(R.id.edtProf);
        EditText mat = findViewById(R.id.edtMatiere);

        Prof prof = new Prof(name.getText().toString(),mat.getText().toString());
        Intent intent = new Intent();
        //Intent intent = getIntent();
        //Log.d("Meslogs",""+intent);
        intent.putExtra("prof",prof);
        setResult(RESULT_OK, intent);
        finish();
    }

}